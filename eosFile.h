#ifndef _EOS_FILE_
#define _EOS_FILE_
#pragma once

#include <string.h>

#include "core\Matrix.h"

#include "engine\GameObject.h"
#include "engine\StringUtils.h"

class Model;
class EOSStorageVolume;

//\brief Representation for a single file
class EOSFile : public GameObject
{
public:
	
	friend class EOSStorageVolume;

	EOSFile() 
		: m_numLayers(0)
		, m_selected(false)
		, m_targetPos(0.0f)
		, m_parentVolume(NULL)
	{ }

	virtual bool Startup();
	virtual bool Draw();
	virtual bool Shutdown();

	virtual bool Initialise(const char * a_fileName);
	virtual void SetSizeBytes(unsigned int a_numBytes);

	inline const char * GetFileName() { return m_fileName; }
	inline const char * GetFilePath() { return m_filePath; }

	inline void SetSelected(bool a_selected = true) { m_selected = a_selected; }
	virtual void Activate(bool a_activate = true);

protected:

	virtual void SetParentVolume(EOSStorageVolume * a_parentVolume) { m_parentVolume = a_parentVolume; }
	
	char m_fileName[StringUtils::s_maxCharsPerName];
	char m_filePath[StringUtils::s_maxCharsPerLine];
	Vector m_targetPos;
	EOSStorageVolume * m_parentVolume;
	unsigned int m_sizeBytes;

private:
	
	void PositionLayers();

	static const unsigned int s_maxLayers = 8;
	static const float sc_layerSize;

	Matrix m_layerMats[s_maxLayers];
	bool m_selected;
	unsigned int m_numLayers;
};

#endif //_EOS_FILE_