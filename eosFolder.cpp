#include "core\MathUtils.h"

#include "engine\FontManager.h"
#include "engine\ModelManager.h"
#include "engine\RenderManager.h"

#include "eosStorageVolume.h"

#include "eosFolder.h"

bool EOSFolder::Update(float a_dt)
{
	// There will be something here eventually
	return GameObject::Update(a_dt);
}

bool EOSFolder::Draw()
{
	Vector textOffset = Vector(-1.0f, 0.0f, -1.5f);
	FontManager::Get().DrawString(m_fileName, StringHash::GenerateCRC("hacker"), 15.0f, GetPos() + textOffset, Colour(0.32f, 0.23f, 0.73f, 0.8f), RenderManager::eBatchWorld);

	return GameObject::Draw();
}

void EOSFolder::SetSizeBytes(unsigned int a_numBytes)
{
	m_sizeBytes = a_numBytes;
}

void EOSFolder::Activate(bool a_activate)
{
	if (m_parentVolume != NULL)
	{
		if (a_activate)
		{
			if (!m_expanded)
			{
				m_parentVolume->Expand(this);
			}
			else
			{
				m_parentVolume->Contract(this);
			}
		}
	}
}