#include "core/Colour.h"
#include "core/MathUtils.h"

#include "engine\RenderManager.h"
#include "engine\TextureManager.h"

#include "eosCamera.h"
#include "eosFloor.h"

const float EOSFloor::sc_density = 32.0f;

void EOSFloor::Startup(Vector a_worldPos, float a_maxAmp, unsigned int a_sizeX, unsigned int a_sizeY)
{ 
	float maxSizeX = a_sizeX * sc_density;
	float maxSizeY = a_sizeY * sc_density;
	m_worldPos = a_worldPos - (maxSizeX * 0.5f);
	m_sizeX = MathUtils::GetMin(a_sizeX, sc_maxPointsX);
	m_sizeY = MathUtils::GetMin(a_sizeY, sc_maxPointsY);
	m_amplitude = a_maxAmp;

	// Load the floor texture
	m_floorTex = TextureManager::Get().GetTexture("floorGrid.tga", TextureManager::eCategoryWorld);

	// The Z vec in the points array is used for animation so we store the world z separately
	m_pointsWorldZ = a_worldPos.GetZ();

	// Generate array of points
	for (unsigned int x = 0; x < m_sizeX; ++x)
	{
		for (unsigned int y = 0; y < m_sizeY; ++y)
		{
			float initialZ = MathUtils::RandFloatRange(-m_amplitude, m_amplitude);
			m_points[x][y].SetX(m_worldPos.GetX() + ((float)x * sc_density));
			m_points[x][y].SetY(m_worldPos.GetY() + ((float)y * sc_density));
			m_points[x][y].SetZ(initialZ);
		}
	}
}

void EOSFloor::Update(float a_dt)
{
	Vector camPos = EOSCamera::Get().GetPos();

	// Update positions of heightmap
	for (unsigned int x = 0; x < m_sizeX; ++x)
	{
		for (unsigned int y = 0; y < m_sizeY; ++y)
		{
			// Scroll with the camera movement to give the illusion of a never ending grid
			if (m_points[x][y].GetY() > -camPos.GetY() + m_sizeY * sc_density * 0.5f)
			{
				m_points[x][y].SetY(m_points[x][y].GetY() - (m_sizeY-1) * sc_density);
			}
			else if (m_points[x][y].GetY() < -camPos.GetY() - m_sizeY * sc_density * 0.5f)
			{
				m_points[x][y].SetY(m_points[x][y].GetY() + (m_sizeY-1) * sc_density);
			}
			else
			{
				// Update Z position of each point
				m_points[x][y].SetZ(m_points[x][y].GetZ() + a_dt);
			}
		}
	}

	Draw();
}

void EOSFloor::Draw()
{
	// Submit lines between each point
	RenderManager & rMan = RenderManager::Get();
	for (unsigned int x = 0; x < m_sizeX-1; ++x)
	{
		for (unsigned int y = 0; y < m_sizeY-1; ++y)
		{
			// The z value is used for animation so we need to convert to worldpos
			RenderManager::eBatch fBat = RenderManager::eBatchWorld;

			Vector realWorldPos = m_points[x][y];
			realWorldPos.SetZ(m_pointsWorldZ + sin(realWorldPos.GetZ()) * m_amplitude);

			Vector realWorldPos1 = m_points[x+1][y];
			realWorldPos1.SetZ(m_pointsWorldZ + sin(realWorldPos1.GetZ()) * m_amplitude);

			Vector realWorldPos2 = m_points[x][y+1];
			realWorldPos2.SetZ(m_pointsWorldZ + sin(realWorldPos2.GetZ()) * m_amplitude);

			const float gridEpsilon = 0.001f;
			if ((realWorldPos1 - realWorldPos2).Length() <= (sc_density*2.0f) - gridEpsilon)
			{
				TexCoord coords[3];
				coords[0] = TexCoord(0.0f, 0.0f);
				coords[1] = TexCoord(1.0f, 0.0f);
				coords[2] = TexCoord(0.0f, 1.0f);
				rMan.AddTri(fBat, realWorldPos, realWorldPos1, realWorldPos2, 
								  coords[0], coords[1], coords[2], m_floorTex);

				Vector realWorldPos3 = m_points[x+1][y+1];
				realWorldPos3.SetZ(m_pointsWorldZ + sin(realWorldPos3.GetZ()) * m_amplitude);
				rMan.AddTri(fBat, realWorldPos3, realWorldPos1, realWorldPos2,
								  coords[0], coords[1], coords[2], m_floorTex);
			}
		}
	}
}

