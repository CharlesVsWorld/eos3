#ifndef _EOS_FOLDER_
#define _EOS_FOLDER_
#pragma once

#include "eosFile.h"

//\brief Representation for a file that contains other files
class EOSFolder : public EOSFile
{

public:

	EOSFolder()
		: m_firstChild(NULL)
		, m_expanded(false)
	{}

	virtual bool Update(float a_dt);
	virtual bool Draw();

	virtual void SetSizeBytes(unsigned int a_numBytes);

	virtual void Activate(bool a_activate = true);

	inline void SetExpanded() { m_expanded = true; }
	inline void SetContracted() { m_expanded = false; }
	inline bool IsExpanded() { return m_expanded; }
	inline bool IsContracted() { return !m_expanded; }
	inline void SetFirstChild(EOSFile * a_child) { m_firstChild = a_child; }
	inline EOSFile * GetFirstChild() { return m_firstChild; }

private:

	EOSFile * m_firstChild;
	bool m_expanded;
};

#endif //_EOS_FOLDER_