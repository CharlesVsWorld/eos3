#include <math.h>

#include "core\MathUtils.h"

#include "engine\Components\ComponentRootMotion.h"
#include "engine\FileManager.h"
#include "engine\ModelManager.h"
#include "engine\RenderManager.h"
#include "engine\TextureManager.h"
#include "engine\WorldManager.h"

#include "eosStorageVolume.h"

const float EOSStorageVolume::sc_minSize = 8.0f;
const float EOSStorageVolume::sc_fileSpacing = 7.0f;

EOSStorageVolume::EOSStorageVolume(const char * a_path, const Vector & a_startPos)
: m_usageBytes(0)
, m_capacityBytes(0)
, m_size(0.0f)
, m_baseTex(NULL)
{
	// Map the root files and folders
	strcpy(m_path, a_path);
	FileManager & fileMan = FileManager::Get();
	ModelManager & modelMan = ModelManager::Get();
	WorldManager & worldMan = WorldManager::Get();
	
	// Get the files on the root of the drive
	FileManager::FileList rootFiles;
	char fullPath[StringUtils::s_maxCharsPerLine];
	fileMan.FillFileList(a_path, rootFiles);
	FileManager::FileListNode * curVolumeFile = rootFiles.GetHead();
	 
	// Set start pos
	m_mat = Matrix::Identity();
	m_mat.SetPos(a_startPos);
	
	// Add all files in the populated list
	while (curVolumeFile != NULL)
	{
		sprintf(fullPath, "%s%s", a_path, curVolumeFile->GetData()->m_name);
		if (curVolumeFile->GetData()->m_isDir)
		{
			AddNewFolder(fullPath);
		}
		else
		{
			AddNewFile(fullPath, curVolumeFile->GetData()->m_sizeBytes);
		}
		curVolumeFile = curVolumeFile->GetNext();
	}

	// Clean up file list
	FileManager::Get().EmptyFileList(rootFiles);

	// Load base tex
	m_baseTex = TextureManager::Get().GetTexture("base.tga", TextureManager::eCategoryWorld);
	
	// Position items and queue them to move
	float additionalZ = 0.0f;
	Vector fileStartPos = m_mat.GetPos();
	fileStartPos.SetZ(fileStartPos.GetZ() + sc_fileSpacing);
	ExpandFilesAndFolders(m_files.GetLength(), m_folders.GetLength(), m_files.GetHead(), m_folders.GetHead(), fileStartPos, m_size, additionalZ);
	
	// Position each side of the volume
	for (unsigned int i = 0; i < sc_numSides; ++i)
	{
		if (m_sides[i] = WorldManager::Get().CreateObject<GameObject>())
		{
			// Set pos and model up
			m_sides[i]->GetWorldMat().SetPos(a_startPos); 
			m_sides[i]->SetName("StorageVolumeSide");
			m_sides[i]->SetModel(ModelManager::Get().GetModel("storageVolumeSides.obj"));
			
			// Add move component for resizes
			m_sides[i]->AddComponent<ComponentRootMotion>();
			ComponentRootMotion * compRM = m_sides[i]->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);

			if (i == 0) 
			{ 
				m_sides[i]->SetWorldMat(m_sides[i]->GetWorldMat().Multiply(Matrix::GetRotateZ(PI)));
				compRM->QueueMove(m_mat.GetPos() + Vector(-m_size, -m_size, 0.0f), 1.0f);
			}
			else if (i == 1) 
			{ 
				m_sides[i]->SetWorldMat(m_sides[i]->GetWorldMat().Multiply(Matrix::GetRotateZ(PI*0.5f)));
				compRM->QueueMove(m_mat.GetPos() + Vector(m_size, -m_size, 0.0f), 1.0f);
			}
			else if (i == 2) 
			{ 
				m_sides[i]->SetWorldMat(m_sides[i]->GetWorldMat().Multiply(Matrix::GetRotateZ(-PI*0.5f)));
				compRM->QueueMove(m_mat.GetPos() + Vector(-m_size, m_size, 0.0f), 1.0f);
			}
			else if (i == 3) 
			{ 
				compRM->QueueMove(m_mat.GetPos() + Vector(m_size, m_size, 0.0f), 1.0f);
			}
		}
	}
}

void EOSStorageVolume::SetSize(float a_newSize)
{
	m_size = a_newSize;
	for (unsigned int i = 0; i < sc_numSides; ++i)
	{
		if (m_sides[i] == NULL)
		{
			return;
		}

		if (m_sides[i]->HasComponent(Component::eComponentTypeRootMotion))
		{
			ComponentRootMotion * compRM = m_sides[i]->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
			switch (i)
			{
				case 0: compRM->QueueMove(m_mat.GetPos() + Vector(-m_size, -m_size, 0.0f), 1.0f); break;
				case 1: compRM->QueueMove(m_mat.GetPos() + Vector(m_size, -m_size, 0.0f), 1.0f); break;
				case 2: compRM->QueueMove(m_mat.GetPos() + Vector(-m_size, m_size, 0.0f), 1.0f); break;
				case 3: compRM->QueueMove(m_mat.GetPos() + Vector(m_size, m_size, 0.0f), 1.0f); break;
				default: break;
			}
		}
	}
}

EOSStorageVolume::FolderNode * EOSStorageVolume::AddNewFolder(const char * a_fullPath, EOSFile * a_parent)
{
	// Create the folder game object
	if (EOSFolder * newFolder = WorldManager::Get().CreateObject<EOSFolder>())
	{
		newFolder->Initialise(a_fullPath);
		newFolder->SetParentVolume(this);
		newFolder->SetModel(ModelManager::Get().GetModel("folder.obj"));
		newFolder->SetClipType(GameObject::eClipTypeAxisBox);
		newFolder->SetClipSize(2.0f);

		// Insert into list
		if (FolderNode * newFolderNode = new FolderNode())
		{
			newFolderNode->SetData(newFolder);
			m_folders.Insert(newFolderNode);
			return newFolderNode;
		}
	}

	return NULL;
}

EOSStorageVolume::FileNode * EOSStorageVolume::AddNewFile(const char * a_fullPath, unsigned int a_sizeBytes, EOSFile * a_parent)
{
	// Create the file game object
	if (EOSFile * newFile = WorldManager::Get().CreateObject<EOSFile>())
	{
		newFile->Initialise(a_fullPath);
		newFile->SetParentVolume(this);
		newFile->SetModel(ModelManager::Get().GetModel("file.obj"));
		newFile->SetSizeBytes(a_sizeBytes);
		newFile->SetClipType(GameObject::eClipTypeAxisBox);
		newFile->SetClipSize(2.0f);
			
		// Insert into list
		FileNode * newFileNode = new FileNode();
		newFileNode->SetData(newFile);
		m_files.Insert(newFileNode);
		return newFileNode;
	}

	return NULL;
}

void EOSStorageVolume::ExpandFilesAndFolders(const unsigned int numFiles, const unsigned int numFolders, FileNode * a_startFiles, FolderNode * a_startFolders, const Vector & a_startPos, float & a_newSize_OUT, float & a_additionalSizeZ)
{
	// Safety checks
	if (numFiles + numFolders == 0 || (a_startFiles == NULL && a_startFolders == NULL))
	{
		return;
	}

	// Calc new size
	const unsigned int numObjects = numFiles + numFolders;
	const unsigned int numObjSq = (unsigned int)sqrt((float)numObjects);
	a_newSize_OUT = (float)(numObjSq - 1) * sc_fileSpacing * 0.5f;
	a_newSize_OUT = MathUtils::GetMax(a_newSize_OUT, sc_minSize);

	// Arrange folders on the bottom
	FolderNode * curFolder = a_startFolders;
	Vector folderStartPos = Vector(a_startPos.GetX() - a_newSize_OUT, a_startPos.GetY() - a_newSize_OUT, a_startPos.GetZ());
	Vector curPos = folderStartPos;
	unsigned int objectCount = 0;
	while (curFolder != NULL)
	{
		// Setup the folder start and end pos
		EOSFolder * curFolderObj = curFolder->GetData();
		curFolderObj->SetPos(a_startPos);
		if (curFolderObj->HasComponent(Component::eComponentTypeRootMotion))
		{
			ComponentRootMotion * compRM = curFolderObj->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
			compRM->QueueMove(curPos, 1.0f);
		}

		// Advance the position for the next folder
		curPos += Vector(sc_fileSpacing, 0.0f, 0.0f);
		curFolder = curFolder->GetNext();
		++objectCount;

		if (objectCount % (numObjSq*numObjSq) == 0)
		{
			curPos = folderStartPos;
			curPos.SetZ(curPos.GetZ() + sc_fileSpacing);
			a_additionalSizeZ += sc_fileSpacing;
		}
		else if (objectCount % numObjSq == 0)
		{
			curPos.SetX(folderStartPos.GetX());
			curPos.SetY(curPos.GetY() + sc_fileSpacing);
		}
	}

	// Remove extra padding if we ended on a remainder
	if (objectCount % (numObjSq*numObjSq) == 0)
	{
		a_additionalSizeZ -= sc_fileSpacing;
	}

	// Now arrange files on top of folders
	objectCount = 0;
	a_additionalSizeZ += sc_fileSpacing;
	Vector fileStartPos(a_startPos.GetX() - a_newSize_OUT, a_startPos.GetY() - a_newSize_OUT, a_startPos.GetZ() + a_additionalSizeZ);
	curPos = fileStartPos;
	FileNode * curFile = a_startFiles;
	while (curFile != NULL)
	{
		// Setup the file start and end pos
		EOSFile * curFileObj = curFile->GetData();
		curFile->GetData()->SetPos(fileStartPos);
		if (curFileObj->HasComponent(Component::eComponentTypeRootMotion))
		{
			ComponentRootMotion * compRM = curFileObj->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
			compRM->QueueMove(curPos, 1.0f);
		}

		// Advance the position for the next file
		curPos += Vector(sc_fileSpacing, 0.0f, 0.0f);
		curFile = curFile->GetNext();
		objectCount++;

		if (objectCount % (numObjSq*numObjSq) == 0)
		{
			curPos = fileStartPos;
			curPos.SetZ(curPos.GetZ() + sc_fileSpacing);
			a_additionalSizeZ += sc_fileSpacing;
		}
		else if (objectCount % numObjSq == 0)
		{
			curPos.SetX(fileStartPos.GetX());
			curPos.SetY(curPos.GetY() + sc_fileSpacing);
		}
	}

	// Add on extra padding if we ended outside a remainder
	if (objectCount % (numObjSq*numObjSq) != 0)
	{
		a_additionalSizeZ += sc_fileSpacing;
	}
	a_additionalSizeZ += sc_fileSpacing;
}

EOSStorageVolume::~EOSStorageVolume()
{
	FileNode * curFile = m_files.GetHead();
	while (curFile != NULL)
	{
		delete curFile->GetData();
		FileNode * next = curFile->GetNext();
		delete curFile;
		curFile = next;
	}
}

void EOSStorageVolume::Update(float a_dt)
{
	// Draw all the folders in the volume
	FolderNode * curFolder = m_folders.GetHead();
	while (curFolder != NULL)
	{
		curFolder->GetData()->Update(a_dt);
		curFolder = curFolder->GetNext();
	}

	// Now the files
	FileNode * curFile = m_files.GetHead();
	while (curFile != NULL)
	{
		curFile->GetData()->Update(a_dt);
		curFile = curFile->GetNext();
	}

	// Draw the base and sides
	Draw();
}

void EOSStorageVolume::Expand(EOSFolder * a_folderToExpand)
{
	if (a_folderToExpand != NULL)
	{
		// Middle management of justice
		FileManager & fileMan = FileManager::Get();
		ModelManager & modelMan = ModelManager::Get();
		WorldManager & worldMan = WorldManager::Get();

		// Create a list of files to move above the expanded files
		LinkedList<EOSFile> filesToMove;
		FolderNode * curFolder = m_folders.GetHead();
		while (curFolder != NULL)
		{
			EOSFolder * curFolderObj = curFolder->GetData();
			if (curFolderObj->GetPos().GetZ() > a_folderToExpand->GetPos().GetZ())
			{
				FileNode * newFile = new FileNode(curFolderObj);
				filesToMove.Insert(newFile);
			}
			curFolder = curFolder->GetNext();
		}
		FileNode * curFile = m_files.GetHead();
		while (curFile != NULL)
		{
			EOSFile * curFileObj = curFile->GetData();
			if (curFileObj->GetPos().GetZ() > a_folderToExpand->GetPos().GetZ())
			{
				FileNode * newFile = new FileNode(curFileObj);
				filesToMove.Insert(newFile);
			}
			curFile = curFile->GetNext();
		}

		// Get the new files and folders in the target path
		FileManager::FileList filesInPath;
		char path[StringUtils::s_maxCharsPerLine];
		char fPath[StringUtils::s_maxCharsPerLine];
		sprintf(path, "%s%s%s", a_folderToExpand->GetFilePath(), a_folderToExpand->GetFileName(), StringUtils::s_charPathSep);
		if (fileMan.FillFileList(path, filesInPath))
		{
			FileManager::FileListNode * curVolumeFile = filesInPath.GetHead();

			// Create all the new objects
			unsigned int numNewFiles = 0;
			unsigned int numNewFolders = 0;
			FolderNode * startFolder = NULL;
			FileNode * startFile = NULL;
			while (curVolumeFile != NULL)
			{
				if (curVolumeFile->GetData()->m_isDir)
				{
					// Add folder and set marker for the first new entry in the linked list
					sprintf(fPath, "%s%s", path, curVolumeFile->GetData()->m_name);
					if (FolderNode * newFolder = AddNewFolder(fPath, a_folderToExpand))
					{
						if (numNewFolders == 0)
						{
							startFolder = newFolder;
						}
						++numNewFolders;
					}
				}
				else
				{
					// Add file and set marker for the first new entry in the linked list
					sprintf(fPath, "%s%s", path, curVolumeFile->GetData()->m_name);
					if (FileNode * newFile = AddNewFile(fPath, curVolumeFile->GetData()->m_sizeBytes, a_folderToExpand))
					{
						if (numNewFiles == 0)
						{
							startFile = newFile;
						}
						++numNewFiles;
					}
				}
				curVolumeFile = curVolumeFile->GetNext();
			}

			// Clean up file list
			FileManager::Get().EmptyFileList(filesInPath);

			// Move the target folder into empty space and change model
			Vector targetPos = Vector(m_mat.GetPos().GetX(), m_mat.GetPos().GetY(), a_folderToExpand->GetPos().GetZ() + sc_fileSpacing);
			ComponentRootMotion * compRM = a_folderToExpand->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
			compRM->QueueMove(targetPos, 1.0f);
			a_folderToExpand->SetModel(ModelManager::Get().GetModel("folder_expanded.obj"));
			a_folderToExpand->SetExpanded();

			// Expand newly added objects
			float newSize(m_size);
			float additionalZ(0.0f);
			Vector aboveExpandedPos = targetPos + Vector(0.0f, 0.0f, sc_fileSpacing);
			ExpandFilesAndFolders(numNewFiles, numNewFolders, startFile, startFolder, aboveExpandedPos, newSize, additionalZ);
			
			// Push folders above the target up and clean up the linked list
			FileNode * curFile = filesToMove.GetHead();
			while (curFile != NULL)
			{
				EOSFile * curFileObj = curFile->GetData();
				ComponentRootMotion * compRM = curFileObj->GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
				compRM->QueueMove(curFileObj->GetPos() + Vector(0.0f, 0.0f, additionalZ), 1.0f);

				FileNode * oldNode = curFile;
				curFile = curFile->GetNext();
				delete oldNode;
			}
			
			// Queue the sides to expand if required
			if (newSize > m_size)
			{
				SetSize(newSize);
			}
		}
	}
}

void EOSStorageVolume::Contract(EOSFolder * a_folderToContract)
{
	if (a_folderToContract != NULL && 
		a_folderToContract->IsExpanded() &&
		a_folderToContract->GetFirstChild() != NULL)
	{
		// Restore original model
		a_folderToContract->SetModel(ModelManager::Get().GetModel("folder.obj"));
		/*
		// Close all children
		EOSFile * curChild = a_folderToContract->GetFirstChild();
		unsigned int childCount = 0;
		float spaceZ(0.0);
		while (curChild != NULL)
		{
			WorldManager::Get().DestroyObject(curChild);
			if (curChild->GetParent() == a_folderToContract)
			{
				curChild = curChild->GetNext();
			}
			else
			{
				curChild = NULL;
			}

			if childCOunt mod this and that)
				spaceZ += sc_fileSpacing;
		}

		// Move everything above the expanded folder back down
		while (allFolders)
		{
			if (curFile.z > a_folderToContract)
			{
				curFile->QueueMove()
			}
		}

		Vector origPos = 
		a_folderToContract->QueueMove()*/
	}
}

void EOSStorageVolume::Draw()
{
	RenderManager & rMan = RenderManager::Get();

	// Draw the base mask
	Vector extents[4];
	Vector maskPos = m_mat.GetPos();
	extents[0] = maskPos + Vector(-m_size*4.0f, -m_size*4.0f, 2.0f);
	extents[1] = maskPos + Vector(m_size*4.0f, -m_size*4.0f, 2.0f);
	extents[2] = maskPos + Vector(m_size*4.0f, m_size*4.0f, 2.0f);
	extents[3] = maskPos + Vector(-m_size*4.0f, m_size*4.0f, 2.0f);
	rMan.AddQuad3D(RenderManager::eBatchWorld, &extents[0], m_baseTex);
}

EOSFile * EOSStorageVolume::GetRandomFile()
{
	// Get a file at a random index
	if (m_files.GetLength() > 0)
	{
		unsigned int randIndex = MathUtils::RandIntRange(0, m_files.GetLength());
		FileNode * curFile = m_files.GetHead();
		unsigned int fileCount = 0;
		while (curFile != NULL)
		{
			if (fileCount++ == randIndex)
			{
				return curFile->GetData();
			}
			curFile = curFile->GetNext();
		}
	}

	return NULL;
}

EOSFolder * EOSStorageVolume::GetRandomFolder()
{
	// Get a folder at a random index
	unsigned int randIndex = MathUtils::RandIntRange(0, m_folders.GetLength());
	FolderNode * curFolder = m_folders.GetHead();
	unsigned int folderCount = 0;
	while (curFolder != NULL)
	{
		if (folderCount++ == randIndex)
		{
			return curFolder->GetData();
		}
		curFolder = curFolder->GetNext();
	}

	return NULL;
}
