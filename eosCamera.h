#ifndef _EOS_CAMERA_
#define _EOS_CAMERA_
#pragma once

#include "core\Matrix.h"
#include "core\Vector.h"

#include "engine\InputManager.h"
#include "engine\Singleton.h"

class EOSCamera : public Singleton<EOSCamera>
{

public:

	//\brief Contruct a valid camera at the origin look down the Y axis
	EOSCamera() 
		: m_pos(Vector::Zero())
		, m_vel(Vector::Zero())
		, m_lookDir(0.0f)
		, m_orientation(0.0f)
	{
		m_look = Vector(0.0f, 1.0f, 0.0f);
		CalculateCameraMatrix();
	}

	//\brief Reset the camera based on a new look at target
	void LookAt(Vector a_worldPos);

	void Update(float a_dt);

	inline void SetVelocity(Vector a_vel)		{ m_vel = a_vel; }
	void AddVelocity(Vector a_velInc)			{ m_vel += a_velInc; }
	inline void ClearVelocity()					{ m_vel = Vector::Zero(); }

	//\brief Accessor for rendering 
	inline Matrix GetMatrix() { return m_mat; }
	inline Vector GetPos() { return m_pos; }
	inline Vector2 GetOrientation() { return m_orientation; }
	inline void	SetLookDir(Vector2 newLookDir) { m_lookDir = newLookDir; }
	inline void SetPos(const Vector & a_newPos) { m_pos = a_newPos; }

	inline Matrix GetViewMatrix() { return m_viewMat; }
	inline Vector GetWorldPos() { return Vector(-m_pos.GetX(), -m_pos.GetY(), -m_pos.GetZ()); }

	static SDLKey GetCameraCancelKey() { return sc_cameraCancelKey; }

private:

	void CalculateCameraMatrix();

	static const SDLKey sc_cameraCancelKey = SDLK_LSHIFT;
	static const float sc_maxRotation;
	static const float sc_maxVelocity;
	static const float sc_decel;

	Matrix m_mat;			// The matrix defining the new coordinate system as calculated by the camera
	Vector m_pos;			// Position of the camera in the world
	Vector m_look;			// World position to look at
	Vector m_vel;			// How fast the camera is traveling in each dimension
	Vector2 m_orientation;	// Angle of the camera around it's view axis
	Vector2 m_lookDir;		// Look direction set by mouse coords
	Matrix m_viewMat;				///< The matrix defining the direction the camera is looking
};

#endif //_EOS_CAMERA_