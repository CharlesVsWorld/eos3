#ifndef _EOS_SENTIENCE_
#define _EOS_SENTIENCE_
#pragma once

#include "core\Vector.h"

#include "engine\GameObject.h"
#include "engine\Singleton.h"

class EOSStorageVolume;

//\brief A sentient servant of the system for better or worse.
class EOSSentience : public GameObject
{
public:

	EOSSentience()
	: m_speed(0.0f) 
	, m_pctGood(0)
	, m_pctEvil(0)
	{ }
	~EOSSentience() { }

	void Init(Vector a_source, GameObject * a_target);

	virtual bool Update(float a_dt);
	
private:

	GameObject * m_target;
	Vector m_source;
	Vector m_dest;
	float m_speed;
	unsigned int m_pctGood;
	unsigned int m_pctEvil;
};

class EOSSentienceManager : public Singleton<EOSSentienceManager>
{
public:

	EOSSentienceManager();

	void Update(float a_dt);

	inline void AddTargetVolume(EOSStorageVolume * a_volume) { m_targetVolumes[m_numTargetVolumes++] = a_volume; }
	inline void AddSource(const Vector & a_sourcePos) { m_sourcePos[m_numSourcePos++] = a_sourcePos; }

private:

	static const unsigned int sc_maxSentience = 64;
	static const unsigned int sc_maxSourcePos = 8;
	static const unsigned int sc_maxTargetVolumes = 64;
	static const float sc_maxCreationDelay;

	Vector m_sourcePos[sc_maxSourcePos];
	EOSSentience * m_sents[sc_maxSentience];
	EOSStorageVolume * m_targetVolumes[sc_maxTargetVolumes];
	float m_creationDelay;
	float m_creationTimer;
	unsigned int m_numSents;
	unsigned int m_numSourcePos;
	unsigned int m_numTargetVolumes;
};

#endif //_EOS_SENTIENCE_