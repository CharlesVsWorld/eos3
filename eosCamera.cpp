#include "core\MathUtils.h"

#include "eosCamera.h"

const float EOSCamera::sc_maxRotation = 360.0f;
const float EOSCamera::sc_maxVelocity = 10.0f;
const float EOSCamera::sc_decel = 1.5f;

template<> EOSCamera * Singleton<EOSCamera>::s_instance = NULL;

void EOSCamera::LookAt(Vector a_worldPos)
{
	m_look = a_worldPos;
}

void EOSCamera::Update(float a_dt)
{
	// Apply velocity to camera position
	InputManager & inMan = InputManager::Get();
	if (!inMan.IsKeyDepressed(sc_cameraCancelKey))
	{
		m_pos += m_vel * a_dt;	

		// Set rotation based on mouse delta while hotkey pressed
		m_orientation.SetX(m_orientation.GetX() + (sc_maxRotation * a_dt * m_lookDir.GetX()) * 0.01f);
		m_orientation.SetY(m_orientation.GetY() + (sc_maxRotation * a_dt * m_lookDir.GetY()) * 0.01f);

		// Tend velocity towards zero
		m_vel.SetX(MathUtils::LerpFloat(m_vel.GetX(), 0.0f, sc_decel * a_dt));
		m_vel.SetY(MathUtils::LerpFloat(m_vel.GetY(), 0.0f, sc_decel * a_dt));
		m_vel.SetZ(MathUtils::LerpFloat(m_vel.GetZ(), 0.0f, sc_decel * a_dt));
	}

	// Create a view matrix without transposition
	m_viewMat = Matrix::Identity();
	m_viewMat = m_viewMat.Multiply(Matrix::GetRotateX(m_orientation.GetY()));
	m_viewMat = m_viewMat.Multiply(Matrix::GetRotateZ(m_orientation.GetX()));

	CalculateCameraMatrix();
}

void EOSCamera::CalculateCameraMatrix()
{
	// Create the matrix
	m_mat = Matrix::Identity();
	m_mat.SetPos(m_pos);

	// Rotate the matrix about two axis defined by the mouse coords
	float angleX = (PI * 0.5f) + m_orientation.GetY();
	float angleY = m_orientation.GetX();
	m_mat = m_mat.Multiply(Matrix::GetRotateZ(-angleY));
	m_mat = m_mat.Multiply(Matrix::GetRotateX(-angleX));
}
