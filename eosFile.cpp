#include "core\MathUtils.h"

#include "engine\Components\ComponentRootMotion.h"
#include "engine\Log.h"
#include "engine\FontManager.h"
#include "engine\ModelManager.h"
#include "engine\RenderManager.h"

#include "eosFile.h"

const float EOSFile::sc_layerSize = 0.35f;

bool EOSFile::Startup()
{
	AddComponent<ComponentRootMotion>();
	return GameObject::Startup();
}

bool EOSFile::Initialise(const char * a_fullPath)
{
	m_sizeBytes = 0;
	m_numLayers = 0;
	
	// Find the last instance of the path seperator
	if (const char * lastPathSep = strrchr(a_fullPath, *StringUtils::s_charPathSep))
	{
		++lastPathSep;
		strcpy(m_fileName, lastPathSep);
		const unsigned int pathLen = strlen(a_fullPath) - strlen(lastPathSep);
		strncpy(m_filePath, a_fullPath, pathLen);
		m_filePath[pathLen] = '\0';

		// Set name on parent for debugging purposes
		GameObject::SetName(m_fileName);
	}

	return true;
}

bool EOSFile::Shutdown()
{
	RemoveComponent(Component::eComponentTypeRootMotion);
	return GameObject::Shutdown();
}

void EOSFile::SetSizeBytes(unsigned int a_numBytes)
{
	m_sizeBytes = a_numBytes;

	unsigned int numLayers = m_sizeBytes > 0 ? m_sizeBytes / 1024000 : 1;

	m_numLayers = MathUtils::GetMin(numLayers, s_maxLayers);

	PositionLayers();
}

void EOSFile::Activate(bool a_activate)
{
	if (m_parentVolume != NULL)
	{
		if (a_activate)
		{
			// TODO Hex editor interface
		}
		else
		{

		}
	}
}

bool EOSFile::Draw()
{
	Vector textOffset = Vector(-1.0f, 0.0f, -1.0f);
	FontManager::Get().DrawString(m_fileName, StringHash::GenerateCRC("hacker"), 15.0f, GetPos() + textOffset, Colour(0.32f, 0.23f, 0.73f, 0.8f), RenderManager::eBatchWorld);

	for (unsigned int i = 0; i < m_numLayers; ++i)
	{
		RenderManager::Get().AddModel(RenderManager::eBatchWorld, GetModel(), &m_layerMats[i]);
	}

	// Reposition layers if necessary
	if (m_numLayers > 0 && m_layerMats[0].GetPos().GetZ() != GetPos().GetZ())
	{
		PositionLayers();
	}

	return GameObject::Draw();
}

void EOSFile::PositionLayers()
{
	// Calc pos for new models
	for (unsigned int i = 0; i < m_numLayers; ++i)
	{
		m_layerMats[i] = GetWorldMat();
		m_layerMats[i].SetPos(GetPos() + Vector(0.0f, 0.0f, i * sc_layerSize));
	}

	// Recaclc clipping volume
	SetClipSize(Vector(2.0f, 2.0f, MathUtils::GetMax(sc_layerSize * m_numLayers, 0.2f)));
	SetClipOffset(Vector(0.0f, 0.0f, sc_layerSize * m_numLayers * 0.5f));
}