#ifndef _EOS_CONTROL_
#define _EOS_CONTROL_
#pragma once

#include "core\Colour.h"

#include "engine\InputManager.h"

class GameObject;

class EOSFile;

class EOSControl
{

public:

	enum eMoveControl
	{
		eMoveControl_None = -1,

		eMoveControl_Forward,
		eMoveControl_Backward,
		eMoveControl_StrafeLeft,
		eMoveControl_StrafeRight,
		eMoveControl_Raise,
		eMoveControl_Lower,
		
		eMoveControl_Count,
	};

	EOSControl();
	~EOSControl() {};

	void Update(float a_dt);

	//\brief Moving the camera around
	inline bool OnControlView_Forward(bool a_enable)		{ m_moveControls[eMoveControl_Forward] = a_enable;		return a_enable; }
	inline bool OnControlView_Backward(bool a_enable)		{ m_moveControls[eMoveControl_Backward] = a_enable;		return a_enable; }
	inline bool OnControlView_StrafeLeft(bool a_enable)		{ m_moveControls[eMoveControl_StrafeLeft] = a_enable;	return a_enable; }
	inline bool OnControlView_StrafeRight(bool a_enable)	{ m_moveControls[eMoveControl_StrafeRight] = a_enable;	return a_enable; }
	inline bool OnControlView_Raise(bool a_enable)			{ m_moveControls[eMoveControl_Raise] = a_enable;		return a_enable; }
	inline bool OnControlView_Lower(bool a_enable)			{ m_moveControls[eMoveControl_Lower] = a_enable;		return a_enable; }

	//\brief Clicking selects things
	bool OnClick(bool a_enable);

	//\brief Activating is a context sensitive action, for files it opens, for folders it expands
	bool OnActivate(bool a_enable);

private:

	void ApplyMoveControl(eMoveControl a_control, float a_dt);

	static const float sc_fMaxVelPerFrame;		///< How much velocity is added to the camera 
	static const float sc_cursorSize;			///< Size of debug mouse cursor
	static const float sc_cursorAnimRate;		///< How fast the cursor pulses

	Vector2 m_vectorCursorNavigation[16];		///< Coords for drawing the cursor when flying around
	Vector2 m_vectorCursorSelection[4];			///< Coords for drawing the cursor when selecting
	Colour m_cursorColours[2];					///< 0 is the cursor outline, 1 is the animated fill colour
	Vector2 m_mouseCoords;						///< Where is the mouse? It is here.
	float m_cursorAnim;							///< Key for the pulsing cursor
	bool m_moveControls[eMoveControl_Count];	///< Which keys are pressed
	GameObject * m_lockIndicator;				///< Where the player's file lock will be placed
	GameObject * m_lock;						///< The player's file lock
	EOSFile * m_selectedObject;					///< The currently selected item mouseover
	EOSFile * m_activeObject;					///< The currently selected item clicked
};

#endif