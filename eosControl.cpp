#include "core/Vector.h"

#include "engine\GameObject.h"
#include "engine\InputManager.h"
#include "engine\ModelManager.h"
#include "engine\RenderManager.h"
#include "engine\WorldManager.h"

#include "eosCamera.h"
#include "eosFile.h"

#include "eosControl.h"

const float EOSControl::sc_fMaxVelPerFrame = 128.0f;
const float EOSControl::sc_cursorSize = 0.05f;
const float EOSControl::sc_cursorAnimRate = 3.0f;

EOSControl::EOSControl()
: m_selectedObject(NULL)
, m_activeObject(NULL)
, m_lockIndicator(NULL)
, m_lock(NULL)
{
	InputManager & inMan = InputManager::Get();

	// Pressing keys
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Forward,			SDLK_w);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Backward,		SDLK_s);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_StrafeLeft,		SDLK_a);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_StrafeRight,		SDLK_d);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Raise,			SDLK_q);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Lower,			SDLK_e);
	inMan.RegisterKeyCallback(this, &EOSControl::OnActivate,					SDLK_SPACE);
	
	// Releasing keys
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Forward,			SDLK_w, InputManager::eInputTypeKeyUp);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Backward,		SDLK_s, InputManager::eInputTypeKeyUp);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_StrafeLeft,		SDLK_a, InputManager::eInputTypeKeyUp);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_StrafeRight,		SDLK_d, InputManager::eInputTypeKeyUp);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Raise,			SDLK_q, InputManager::eInputTypeKeyUp);
	inMan.RegisterKeyCallback(this, &EOSControl::OnControlView_Lower,			SDLK_e, InputManager::eInputTypeKeyUp);

	// Clicking the mouse
	inMan.RegisterMouseCallback(this, &EOSControl::OnClick, InputManager::eMouseButtonLeft);

	// Initialise all keys to not pressed
	memset(m_moveControls, 0, sizeof(bool) * eMoveControl_Count);
	m_mouseCoords = Vector2::Vector2Zero();

	// Set up cursor for selection
	const float aspect = RenderManager::Get().GetViewAspect();
	m_vectorCursorSelection[0] = Vector2(0.0f, 0.0f);
	m_vectorCursorSelection[1] = Vector2(sc_cursorSize, -sc_cursorSize*aspect);
	m_vectorCursorSelection[2] = Vector2(sc_cursorSize*0.3f, -sc_cursorSize*0.7f*aspect);
	m_vectorCursorSelection[3] = Vector2(0.0f, -sc_cursorSize*aspect);
	
	// Set up cursor for navigation
	m_vectorCursorNavigation[0] = Vector2(-sc_cursorSize, sc_cursorSize*0.5f*aspect);
	m_vectorCursorNavigation[1] = Vector2(-sc_cursorSize, sc_cursorSize*0.75f*aspect);
	m_vectorCursorNavigation[2] = Vector2(-sc_cursorSize*0.75f, sc_cursorSize*aspect);
	m_vectorCursorNavigation[3] = Vector2(-sc_cursorSize*0.5f, sc_cursorSize*aspect);
	
	m_vectorCursorNavigation[4] = Vector2(sc_cursorSize*0.5f, sc_cursorSize*aspect);
	m_vectorCursorNavigation[5] = Vector2(sc_cursorSize*0.75f, sc_cursorSize*aspect);
	m_vectorCursorNavigation[6] = Vector2(sc_cursorSize, sc_cursorSize*0.75f*aspect);
	m_vectorCursorNavigation[7] = Vector2(sc_cursorSize, sc_cursorSize*0.5f*aspect);

	m_vectorCursorNavigation[8] = Vector2(sc_cursorSize, -sc_cursorSize*0.5f*aspect);
	m_vectorCursorNavigation[9] = Vector2(sc_cursorSize, -sc_cursorSize*0.75f*aspect);
	m_vectorCursorNavigation[10] = Vector2(sc_cursorSize*0.75f, -sc_cursorSize*aspect);
	m_vectorCursorNavigation[11] = Vector2(sc_cursorSize*0.5f, -sc_cursorSize*aspect);

	m_vectorCursorNavigation[12] = Vector2(-sc_cursorSize*0.5f, -sc_cursorSize*aspect);
	m_vectorCursorNavigation[13] = Vector2(-sc_cursorSize*0.75f, -sc_cursorSize*aspect);
	m_vectorCursorNavigation[14] = Vector2(-sc_cursorSize, -sc_cursorSize*0.75f*aspect);
	m_vectorCursorNavigation[15] = Vector2(-sc_cursorSize, -sc_cursorSize*0.5f*aspect);

	// And colours
	m_cursorAnim = 0.0f;
	m_cursorColours[0] = sc_colourGreen;
	m_cursorColours[1] = Colour(sc_colourGreen.GetR(), sc_colourGreen.GetG(), sc_colourGreen.GetB(), sc_colourGreen.GetA()*1.0f);

	// Create the lock objects
	WorldManager & worldMan = WorldManager::Get();
	ModelManager & modelMan = ModelManager::Get();
	m_lock = worldMan.CreateObject<GameObject>();
	m_lockIndicator = worldMan.CreateObject<GameObject>();

	//Setup their properties
	if (m_lock != NULL && m_lockIndicator != NULL)
	{
		m_lock->SetModel(modelMan.GetModel("lock.obj"));
		m_lockIndicator->SetModel(modelMan.GetModel("lockIndicator.obj"));
		m_lock->SetSleeping();
		m_lockIndicator->SetSleeping();
	}
	else
	{
		Log::Get().WriteGameErrorNoParams("Player's locks unable to be created.");
	}	
}


void EOSControl::Update(float a_dt)
{
	InputManager & inMan = InputManager::Get();
	RenderManager & renMan = RenderManager::Get();
	Vector2 mousePos = inMan.GetMousePosRelative();
	const float aspect = RenderManager::Get().GetViewAspect();
	EOSCamera::Get().SetLookDir(mousePos);	

	// Cycle the cursor alpha
	m_cursorAnim += a_dt * sc_cursorAnimRate;
	m_cursorColours[1].SetA((-sinf(m_cursorAnim)*0.1f) + 0.2f);

	if (!inMan.IsKeyDepressed(EOSCamera::GetCameraCancelKey()))
	{
		for (unsigned int i = 0; i < eMoveControl_Count; ++i)
		{
			if (m_moveControls[i])
			{
				ApplyMoveControl((eMoveControl)i, a_dt);
			}
		}
	
		// Draw the navigation mouse cursor at the center of the screen
		for (int i = 0; i < 15; ++i)
		{
			if (i != 3 && i !=7 && i != 11)
			{
				renMan.AddLine2D(RenderManager::eBatchDebug2D, m_vectorCursorNavigation[i], m_vectorCursorNavigation[i+1], m_cursorColours[0]);
			}
		}
		
		Vector2 cursorFillMiddle[4] = {	Vector2(-sc_cursorSize*0.875f, sc_cursorSize*0.875f*aspect),
										Vector2(sc_cursorSize*0.875f, sc_cursorSize*0.875f*aspect),
										Vector2(sc_cursorSize*0.875f, -sc_cursorSize*0.875f*aspect),
										Vector2(-sc_cursorSize*0.875f, -sc_cursorSize*0.875f*aspect)};
		// Now the middle
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFillMiddle[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);

		// Fill in starting with top flap
		Vector2 cursorFill[4] = {cursorFillMiddle[0], m_vectorCursorNavigation[2], m_vectorCursorNavigation[5], cursorFillMiddle[1]};
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFill[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);
		
		// Right flap
		cursorFill[0] = cursorFillMiddle[1];
		cursorFill[1] = m_vectorCursorNavigation[6];
		cursorFill[2] = m_vectorCursorNavigation[9];
		cursorFill[3] = cursorFillMiddle[2];
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFill[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);

		// Bottom flap
		cursorFill[0] = cursorFillMiddle[2];
		cursorFill[1] = m_vectorCursorNavigation[10];
		cursorFill[2] = m_vectorCursorNavigation[13];
		cursorFill[3] = cursorFillMiddle[3];
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFill[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);
		
		// Left flap
		cursorFill[0] = cursorFillMiddle[3];
		cursorFill[1] = m_vectorCursorNavigation[14];
		cursorFill[2] = m_vectorCursorNavigation[1];
		cursorFill[3] = cursorFillMiddle[0];
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFill[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);
		
		mousePos = Vector2::Vector2Zero();
	}
	else // Draw the selection mouse cursor
	{
		RenderManager & renMan = RenderManager::Get();
		for (int i = 0; i < 3; ++i)
		{
			renMan.AddLine2D(RenderManager::eBatchDebug2D, mousePos+m_vectorCursorSelection[i], mousePos+m_vectorCursorSelection[i+1], m_cursorColours[0]);
		}
		renMan.AddLine2D(RenderManager::eBatchDebug2D, mousePos+m_vectorCursorSelection[3], mousePos+m_vectorCursorSelection[0], m_cursorColours[0]);

		// Fill in
		Vector2 cursorFill[4] = {m_vectorCursorSelection[0] + mousePos, m_vectorCursorSelection[1] + mousePos, m_vectorCursorSelection[2] + mousePos, m_vectorCursorSelection[3] + mousePos};
		renMan.AddQuad2D(RenderManager::eBatchDebug2D, &cursorFill[0], NULL, TexCoord(0.0f, 0.0f), TexCoord(1.0f, 1.0f), Texture::eOrientationNormal, m_cursorColours[1]);
	}

	// Do picking with all the objects in the scene
	if (Scene * curScene = WorldManager::Get().GetCurrentScene())
	{
		const float pickDepth = 100.0f;
		const float persp = 0.47f;
		Matrix camMat = EOSCamera::Get().GetViewMatrix();
		Vector camPos = EOSCamera::Get().GetWorldPos();
		Vector mouseInput = Vector(	mousePos.GetX() * renMan.GetViewAspect() * pickDepth * persp, 
									0.0f, 
									mousePos.GetY() * pickDepth * persp);
		Vector pickEnd = camPos + camMat.GetLook() * pickDepth;
		pickEnd += camMat.Transform(mouseInput);	

		// Pick an arbitrary object (would have to sort to get the closest)
		if (GameObject * curItem = curScene->GetSceneObject(camPos, pickEnd))
		{
			if (m_selectedObject = dynamic_cast<EOSFile *>(curItem))
			{
				FontManager::Get().DrawString(m_selectedObject->GetFileName(), StringHash::GenerateCRC("hacker"), 3.0f,  Vector2(-0.5f, -0.8f), Colour(0.02f, 0.93f, 0.03f, 0.5f), RenderManager::eBatchGui);
				m_lockIndicator->SetPos(m_selectedObject->GetPos());
				m_lockIndicator->SetActive();
			}
		}
		else // Nothing selected
		{
			m_lockIndicator->SetSleeping();
			m_selectedObject = NULL;
		}
	}
}

bool EOSControl::OnClick(bool a_enable)
{
	if (m_selectedObject != NULL)
	{
		// Set the active object for the activate command
		m_lock->SetPos(m_selectedObject->GetPos());
		m_lock->SetActive();
		m_activeObject = m_selectedObject;
		m_activeObject->SetSelected();
		return true;
	}
	else // Clear the selection
	{
		if (m_activeObject != NULL)
		{
			m_lock->SetSleeping();
			m_activeObject->SetSelected(false);
		}
		m_activeObject = NULL;
	}
	return false;
}

bool EOSControl::OnActivate(bool a_enable)
{
	if (m_activeObject != NULL)
	{
		m_lock->SetSleeping();
		m_activeObject->Activate();
		return true;
	}

	return false;
}

void EOSControl::ApplyMoveControl(eMoveControl a_control, float a_dt)
{
	// Create a view direction matrix
	Matrix viewMat = Matrix::Identity();
	viewMat = viewMat.Multiply(Matrix::GetRotateZ(EOSCamera::Get().GetOrientation().GetX()));
	viewMat = viewMat.Multiply(Matrix::GetRotateX(EOSCamera::Get().GetOrientation().GetY()));

	// WSAD style controls
	switch (a_control)
	{
		case eMoveControl_Forward:		EOSCamera::Get().AddVelocity(viewMat.GetLook() * a_dt * -sc_fMaxVelPerFrame); break;
		case eMoveControl_Backward:		EOSCamera::Get().AddVelocity(viewMat.GetLook() * a_dt * sc_fMaxVelPerFrame); break;
		case eMoveControl_StrafeLeft:	EOSCamera::Get().AddVelocity(viewMat.GetRight() * a_dt * sc_fMaxVelPerFrame); break;
		case eMoveControl_StrafeRight:	EOSCamera::Get().AddVelocity(viewMat.GetRight() * a_dt *  -sc_fMaxVelPerFrame); break;
		case eMoveControl_Raise:		EOSCamera::Get().AddVelocity(Vector(0.0f, 0.0f, a_dt * -sc_fMaxVelPerFrame)); break;
		case eMoveControl_Lower:		EOSCamera::Get().AddVelocity(Vector(0.0f, 0.0f, a_dt * sc_fMaxVelPerFrame)); break;
		default: break;
	}
}
