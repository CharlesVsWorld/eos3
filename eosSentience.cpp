#include "core\MathUtils.h"

#include "engine\Components\ComponentRootMotion.h"
#include "engine\ModelManager.h"
#include "engine\WorldManager.h"

#include "eosSystem.h"
#include "eosSentience.h"

template<> EOSSentienceManager * Singleton<EOSSentienceManager>::s_instance = NULL;

const float EOSSentienceManager::sc_maxCreationDelay = 3.0f;

EOSSentienceManager::EOSSentienceManager()
: m_creationTimer(0.0f)
, m_numTargetVolumes(0)
, m_numSents(0)
, m_numSourcePos(0)
{
	for (unsigned int i = 0; i < sc_maxSourcePos; ++i)
	{
		m_sourcePos[i] = Vector::Zero();
	}

	for (unsigned int i = 0; i < sc_maxSentience; ++i)
	{
		m_sents[i] = NULL;
	}

	for (unsigned int i = 0; i < sc_maxTargetVolumes; ++i)
	{
		m_targetVolumes[i] = NULL;
	}

	m_creationDelay = MathUtils::RandFloatRange(0.0f, sc_maxCreationDelay);
}

void EOSSentienceManager::Update(float a_dt)
{
	m_creationTimer += a_dt;

	if (m_creationTimer >= m_creationDelay && 
		m_numTargetVolumes > 0 && 
		m_numSourcePos > 0 &&
		m_numSents < sc_maxSentience)
	{
		// Pick a random file to attack
		unsigned int randVolume = MathUtils::RandIntRange(0, m_numTargetVolumes-1);
		GameObject * randFile = m_targetVolumes[randVolume]->GetRandomFile();
		
		// Failing that a random folder
		if (randFile == NULL)
		{
			randFile =  m_targetVolumes[randVolume]->GetRandomFolder();
		}

		// Send a sentience out to attack
		if (randFile != NULL)
		{
			if (EOSSentience * newSent = WorldManager::Get().CreateObject<EOSSentience>())
			{
				unsigned int randSource = MathUtils::RandIntRange(0, m_numSourcePos-1);
				newSent->Init(m_sourcePos[randSource], randFile);
				m_sents[m_numSents++] = newSent;
			}
		}
		m_creationTimer = 0.0f;
		m_creationDelay = MathUtils::RandFloatRange(0.0f, sc_maxCreationDelay);
	}
}

void EOSSentience::Init(Vector a_source, GameObject * a_target)
{
	if (a_target != NULL)
	{
		m_target = a_target;
		m_source = a_source;
		m_dest = a_target->GetPos();
	
		SetModel(ModelManager::Get().GetModel("sentience.obj"));
		SetPos(m_source);

		// Queue move from source to dest and back
		if (AddComponent<ComponentRootMotion>())
		{
			ComponentRootMotion * compMove = GetComponent<ComponentRootMotion>(Component::eComponentTypeRootMotion);
			compMove->QueueMove(m_dest, MathUtils::RandFloatRange(10.0f, 1000.0f), 0.0f);
			compMove->QueueMove(m_source, MathUtils::RandFloatRange(10.0f, 1000.0f), 0.0f);
		}
	}
}
	
bool EOSSentience::Update(float a_dt)
{
	// TODO
	return GameObject::Update(a_dt);
}
	