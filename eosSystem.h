#ifndef _EOS_SYSTEM_
#define _EOS_SYSTEM_
#pragma once

#include "core\LinkedList.h"
#include "core\Vector.h"

#include "engine\Singleton.h"

#include "eosStorageVolume.h"

class GameObject;
class Texture;

//\brief System/machine information and representation. Windows implementation only (so far)
class EOSSystem : public Singleton<EOSSystem>
{

public:

	EOSSystem()
		: m_numCPUCores(0)
		, m_numGPUs(0)
		, m_cpuClockSpeed(0)
		, m_gpuClockSpeed(0)
		, m_memoryBytes(0)
		, m_cpuBaseTex(NULL)
	{ 
		memset(m_cpuCores, 0, sizeof(GameObject *) * sc_maxCPUs);
		memset(m_cpuOuterCores, 0, sizeof(GameObject *) * sc_maxCPUs);
		memset(m_cpuPlinths, 0, sizeof(GameObject *) * sc_maxCPUs);
		Startup(); 
	}
	~EOSSystem() { Shutdown(); }

	void Startup();
	void Shutdown();
	void Update(float a_dt);
	
	EOSStorageVolume * GetStorageVolume(const char * a_path);
	Vector GetCPUCorePos(unsigned int a_cpuCoreIndex);
	
private:

	typedef LinkedListNode<EOSStorageVolume> StorageVolumeNode;

	static const unsigned int sc_maxCPUs = 16;

	unsigned int m_numCPUCores;
	unsigned int m_numGPUs;

	unsigned int m_cpuClockSpeed;
	unsigned int m_gpuClockSpeed;

	unsigned int m_memoryBytes;

	LinkedList<EOSStorageVolume> m_storageVolumes;

	Texture *	 m_cpuBaseTex;
	GameObject * m_cpuCores[sc_maxCPUs];
	GameObject * m_cpuOuterCores[sc_maxCPUs];
	GameObject * m_cpuPlinths[sc_maxCPUs];
};

#endif //_EOS_SYSTEM_INFO_