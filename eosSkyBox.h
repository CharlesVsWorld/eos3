#ifndef _EOS_SKYBOX_
#define _EOS_SKYBOX_
#pragma once

#include "core\Matrix.h"

class Model;

//\brief Representation for a single file
class EOSSkyBox
{
public:

	EOSSkyBox();
	~EOSSkyBox();
	virtual void Update(float a_dt);
	
	inline void SetPos(Vector a_newPos) { m_worldMat.SetPos(Vector(-a_newPos.GetX(), -a_newPos.GetY(), m_worldMat.GetPos().GetZ())); }

private:

	virtual void Draw();

	static const float sc_baseRotationSpeed;

	Matrix m_worldMat;
	Model * m_model;
};

#endif //_EOS_SKYBOX_