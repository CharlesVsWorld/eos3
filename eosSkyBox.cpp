#include "engine\ModelManager.h"
#include "engine\RenderManager.h"

#include "engine\FontManager.h"

#include "eosCamera.h"
#include "eosSkyBox.h"

const float EOSSkyBox::sc_baseRotationSpeed = 0.1f;

EOSSkyBox::EOSSkyBox()
: m_worldMat(Matrix::Identity())
{
	// Load the model
	m_model = ModelManager::Get().GetModel("skyBoxStars.obj");
}

EOSSkyBox::~EOSSkyBox()
{

}

void EOSSkyBox::Update(float a_dt)
{
	// Update rotation

	// Draw
	Draw();
}

void EOSSkyBox::Draw()
{
	RenderManager & rMan = RenderManager::Get();
	rMan.AddModel(RenderManager::eBatchWorld, m_model, &m_worldMat);
}
