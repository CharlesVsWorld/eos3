#ifndef _EOS_STORAGE_VOLUME
#define _EOS_STORAGE_VOLUME_
#pragma once

#include "core\LinkedList.h"
#include "core\Matrix.h"

#include "eosFile.h"
#include "eosFolder.h"

class Model;
class Texture;

//\brief Representation of a mapped drive like hard drive partition or memory stick
class EOSStorageVolume
{

public:

	EOSStorageVolume(const char * a_path, const Vector & a_startPos);
	~EOSStorageVolume();

	void Update(float a_dt);

	inline const char * GetPath() { return m_path; }

	void Expand(EOSFolder * a_folderToExpand);
	void Contract(EOSFolder * a_folderToContract);
	EOSFile * GetRandomFile();
	EOSFolder * GetRandomFolder();
	
private:

	//\brief Alias for linked list node types
	typedef LinkedListNode<EOSFile> FileNode;
	typedef LinkedListNode<EOSFolder> FolderNode;

	void SetSize(float a_newSize);

	//\brief Helper function to add a named folder to the list
	FolderNode * AddNewFolder(const char * a_fullPath, EOSFile * a_parent = NULL);

	//\brief Helper function to add a named folder to the list
	FileNode * AddNewFile(const char * a_fullPath, unsigned int a_sizeBytes, EOSFile * a_parent = NULL);

	//\brief Helper function to figure out the positions of new files and folders in the volume
	//\param How many new files to expand
	//\param How many new folders to expand
	//\param Where to base the expansion
	//\param OUT The new square dimension of the storage volume after the expansion
	void ExpandFilesAndFolders(const unsigned int numFiles, const unsigned int numFolders, FileNode * a_startFiles, FolderNode * a_startFolders, const Vector & a_startPos, float & a_newSize_OUT, float & a_additionalSizeZ);

	void Draw();

	static const unsigned int sc_numSides = 4;
	static const float sc_minSize;
	static const float sc_fileSpacing;

	char m_path[StringUtils::s_maxCharsPerLine];
	unsigned int m_usageBytes;
	unsigned int m_capacityBytes;

	LinkedList<EOSFile> m_files;
	LinkedList<EOSFolder> m_folders;

	Matrix m_mat;
	GameObject * m_sides[sc_numSides];
	Texture * m_baseTex;
	float m_size;
};

#endif //_EOS_STORAGE_VOLUME_
