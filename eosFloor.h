#ifndef _EOS_FLOOR_
#define _EOS_FLOOR_
#pragma once

#include "core\Vector.h"

class Texture;

//\brief Render a section of heighmap points connected in a grid that undulate mesmerisingly
class EOSFloor
{

public:

	EOSFloor() : m_floorTex(NULL) {}

	void Startup(Vector a_worldPos, float a_maxAmp, unsigned int a_sizeX, unsigned int a_sizeY);

	void Update(float a_dt);

	void SetAmplitude(float a_newAmplitude) { m_amplitude = a_newAmplitude; }

private:

	void Draw();

	static const float sc_density;
	static const unsigned int sc_maxPointsX = 100;
	static const unsigned int sc_maxPointsY = 100;
		
	Vector m_points[sc_maxPointsX][sc_maxPointsY];
	Texture * m_floorTex;
	float m_pointsWorldZ;
	Vector m_worldPos;
	float m_amplitude;
	unsigned int m_sizeX;
	unsigned int m_sizeY;

};

#endif