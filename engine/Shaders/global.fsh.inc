const char * globalFragmentShader = 
"uniform sampler2D Texture0; \n"
"uniform float Time; \n"
"uniform float FrameTime; \n"
"uniform float ViewWidth; \n"
"uniform float ViewHeight; \n"
"varying vec2 OutTexCoord; \n";