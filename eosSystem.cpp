#include "core\MathUtils.h"
#include "core\Vector.h"

#include "engine\GameObject.h"
#include "engine\ModelManager.h"
#include "engine\Log.h"
#include "engine\TextureManager.h"
#include "engine\RenderManager.h"
#include "engine\WorldManager.h"

#include "eosSentience.h"
#include "eosSystem.h"

template<> EOSSystem * Singleton<EOSSystem>::s_instance = NULL;

#if __WIN32__

#include <windows.h>
#include <direct.h>
#include <stdio.h>
#include <tchar.h>

void EOSSystem::Startup()
{
	// Get CPU number of cores
	SYSTEM_INFO sysInfo;
    GetSystemInfo(&sysInfo);
	m_numCPUCores = sysInfo.dwNumberOfProcessors;
	WorldManager & worldMan = WorldManager::Get();
	ModelManager & modelMan = ModelManager::Get();
	EOSSentienceManager & sentMan = EOSSentienceManager::Get();

	// Positioning info
	Vector cpuStartPos(0.0f, 0.0f, 2.0f);
	const float cpuWidth = 50.0f;

	// Load base tex
	m_cpuBaseTex = TextureManager::Get().GetTexture("base.tga", TextureManager::eCategoryWorld);
    for (unsigned int i = 0; i < m_numCPUCores; ++i)
	{
		m_cpuCores[i] = worldMan.CreateObject<GameObject>();
		m_cpuCores[i]->SetModel(modelMan.GetModel("cpu_core.obj"));
		m_cpuCores[i]->SetPos(cpuStartPos);

		m_cpuOuterCores[i] = worldMan.CreateObject<GameObject>();
		m_cpuOuterCores[i]->SetModel(modelMan.GetModel("cpu_outer_core.obj"));
		m_cpuOuterCores[i]->SetPos(cpuStartPos);

		m_cpuPlinths[i] = worldMan.CreateObject<GameObject>();
		m_cpuPlinths[i]->SetModel(modelMan.GetModel("cpu_plinth.obj"));
		m_cpuPlinths[i]->SetPos(cpuStartPos);

		// Advance the cpu pos
		if ((i+1) % 2 == 0)
		{
			cpuStartPos = Vector(cpuStartPos.GetX() + cpuWidth*0.5f, cpuStartPos.GetY() + cpuWidth*0.5f, cpuStartPos.GetZ());
		}
		else
		{
			cpuStartPos = Vector(cpuStartPos.GetX() - cpuWidth, cpuStartPos.GetY() - cpuWidth, cpuStartPos.GetZ());
		}

		// Register as a source of sentience
		sentMan.AddSource(cpuStartPos + Vector(0.0f, 0.0f, 100.0f));
	}

	// GetLogicalDrives returns a bitmask of the available drives
	char driveLetter = 'A';
	DWORD uDriveMask = GetLogicalDrives();
	if(uDriveMask == 0)
	{
		Log::Get().WriteGameErrorNoParams("Cannot get details of logical drives.");
		return;
	}

	unsigned int numDrives = 0;
	Vector driveStartPos(cpuStartPos.GetX() + 100.0f, cpuStartPos.GetY() + 100.0f, 2.0f);
	const float volumeWidth = 32.0f;
	const float volumeRadius = 300.0f;
	Matrix volumeMat = Matrix::Identity();
	volumeMat.SetPos(cpuStartPos);
	while(uDriveMask)
	{
		if(uDriveMask & 1)
		{
			// Construct path from drive letter
			char pathBuf[StringUtils::s_maxCharsPerName];
			sprintf(pathBuf, "%c:\\", driveLetter);

			// Get all folders in the root
			FileManager & fileMan = FileManager::Get();
			FileManager::FileList rootFiles;
			char fullPath[StringUtils::s_maxCharsPerLine];
			fileMan.FillFileList(pathBuf, rootFiles);
			FileManager::FileListNode * curVolumeFile = rootFiles.GetHead();
			const unsigned int numFolders = fileMan.CountFiles(rootFiles, false, true);
	 
			// Add all files in the populated list
			unsigned int folderCount = 0;
			while (curVolumeFile != NULL)
			{
				sprintf(fullPath, "%s%s%s", pathBuf, curVolumeFile->GetData()->m_name, StringUtils::s_charPathSep);
				if (curVolumeFile->GetData()->m_isDir)
				{
					// Set start pos
					const float rotAngle = TAU * ((float)folderCount / (float)numFolders);
					Matrix newVolMat = volumeMat.Multiply(volumeMat.GetRotateZ(rotAngle));
					newVolMat.Translate(newVolMat.Transform(Vector(volumeRadius, 0.0f, 0.0f)));
					newVolMat.Translate(cpuStartPos);

					// Check permissions/access to folder
					if (fileMan.CheckFilePath(fullPath))
					{
						// Add a physical volume
						StorageVolumeNode * newVolume = new StorageVolumeNode();
						newVolume->SetData(new EOSStorageVolume(fullPath, newVolMat.GetPos()));
						m_storageVolumes.Insert(newVolume);
						++folderCount;

						// Register sentience target
						sentMan.AddTargetVolume(newVolume->GetData());
					}
				}
				curVolumeFile = curVolumeFile->GetNext();
			}

			// Clean up file list
			FileManager::Get().EmptyFileList(rootFiles);

			// Advance the drive pos
			driveStartPos += Vector(0.0f, 0.0f, 50.0f);
			++numDrives;
		}

		// Shift the bitmask binary right
		++driveLetter;
		uDriveMask >>= 1;
	}
}
#else
void EOSSystemInfo::Startup()
{
	// TODO implementation on other platforms
	m_numCPUs = 1;
}
#endif

void EOSSystem::Shutdown()
{
	StorageVolumeNode * curNode = m_storageVolumes.GetHead();
	while (curNode != NULL)
	{
		delete curNode->GetData();
		StorageVolumeNode * nextNode = curNode->GetNext();
		delete curNode;
		curNode = nextNode;
	}
}

void EOSSystem::Update(float a_dt)
{
	StorageVolumeNode * curNode = m_storageVolumes.GetHead();
	while (curNode != NULL)
	{
		curNode->GetData()->Update(a_dt);
		curNode = curNode->GetNext();
	}

	// Spin the cpu cores
	const float cpuSize = 50.0f;
	const float rotAmount = a_dt * 25.0f;
	Matrix coreMat = m_cpuCores[0]->GetWorldMat().Multiply(Matrix::GetRotateZ(rotAmount));
	Matrix outerCoreMat = m_cpuOuterCores[0]->GetWorldMat().Multiply(Matrix::GetRotateZ(-rotAmount*0.1f));
	for (unsigned int i = 0; i < m_numCPUCores; ++i)
	{
		Vector origPos = m_cpuCores[i]->GetPos();
		m_cpuCores[i]->SetWorldMat(coreMat);
		m_cpuCores[i]->SetPos(origPos);

		origPos = m_cpuOuterCores[i]->GetPos();
		m_cpuOuterCores[i]->SetWorldMat(outerCoreMat);
		m_cpuOuterCores[i]->SetPos(origPos);

		Vector extents[4];
		extents[0] = Vector(origPos.GetX() - cpuSize, origPos.GetY() - cpuSize, origPos.GetZ());
		extents[1] = Vector(origPos.GetX() + cpuSize, origPos.GetY() - cpuSize, origPos.GetZ());
		extents[2] = Vector(origPos.GetX() + cpuSize, origPos.GetY() + cpuSize, origPos.GetZ());
		extents[3] = Vector(origPos.GetX() - cpuSize, origPos.GetY() + cpuSize, origPos.GetZ());
		RenderManager::Get().AddQuad3D(RenderManager::eBatchWorld, &extents[0], m_cpuBaseTex);
	}
}

EOSStorageVolume * EOSSystem::GetStorageVolume(const char * a_path)
{
	StorageVolumeNode * curNode = m_storageVolumes.GetHead();
	while (curNode != NULL)
	{
		if (strcmp(curNode->GetData()->GetPath(), a_path) != NULL)
		{
			return curNode->GetData();
		}
		curNode = curNode->GetNext();
	}

	return NULL;
}

Vector EOSSystem::GetCPUCorePos(unsigned int a_cpuCoreIndex)
{
	if (m_numCPUCores > a_cpuCoreIndex)
	{
		return m_cpuCores[a_cpuCoreIndex]->GetPos() + Vector(0.0f, 0.0f, 90.0f);
	}

	return Vector::Zero();
}